SRCDIR = src

TARGET = main

OBJDIR = build

SRC =  $(SRCDIR)/$(TARGET).c
SRC += $(SRCDIR)/spi_driver.c
SRC += $(SRCDIR)/system_init.c
SRC += src/glcd/devices/AVR8.c
SRC += src/glcd/glcd.c
SRC += src/glcd/controllers/ST7565R.c
SRC += src/glcd/graphics.c
SRC += src/glcd/graphs.c
SRC += src/glcd/text.c
SRC += src/glcd/text_tiny.c
SRC += src/glcd/unit_tests.c

INCLUDES = $(SRCDIR)/glcd

MCU = atmega2560

F_CPU = 16000000

CSTANDARD = -std=gnu99

GLCD = -D GLCD_CONTROLLER_ST7565R
GLCD += -D GLCD_USE_PARALLEL
GLCD += -D GLCD_INIT_NHD_C12864A1Z_FSW_FBW_HTT
GLCD += -D GLCD_DEVICE_AVR8
GLCD += -D GLCD_USE_AVR_DELAY
GLCD += -D __DELAY_BACKWARD_COMPATIBLE__

CDEFS = -DF_CPU=$(F_CPU)UL

CFLAGS = -Os
CFLAGS += -mmcu=$(MCU)
CFLAGS += $(CDEFS)
CFLAGS += -Wall
CFLAGS += $(CSTANDARD)

LDFLAGS = -Wl,-Map=$(OBJDIR)/$(TARGET).map

CC = avr-gcc

OBJ = $(SRC:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

$(OBJDIR)/%.o : $(SRCDIR)/%.c 
	$(CC) -c $(CFLAGS) $(GLCD) $< -o $@

.PHONY: build
build: all

.PHONY:all
all:$(OBJDIR)/$(TARGET).elf

$(OBJDIR)/$(TARGET).elf: $(OBJ)
	$(CC) $(OBJ) $(CFLAGS)  $(GLCD) $(LDFLAGS) -o $@

.PHONY:clean
clean:
	rm -f $(OBJ) $(OBJDIR)/$(TARGET).elf  $(OBJDIR)/$(TARGET).map 

#$(OBJDIR)/%.hex: $(OBJDIR)/%.elf
