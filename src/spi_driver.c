#include <stdint.h>
#include <avr/io.h>

#define PORT_SPI    PORTB
#define DDR_SPI     DDRB
#define DD_MISO     DDB3
#define DD_MOSI     DDB2
#define DD_SS       DDB0
#define DD_SCK      DDB1 
 
void spi_driver_init_master(void)
// Initialize pins for spi communication
{
    DDR_SPI &= ~((1<<DD_MOSI)|(1<<DD_MISO)|(1<<DD_SS)|(1<<DD_SCK));
// Define the following pins as output
    DDR_SPI |= ((1<<DD_MOSI)|(1<<DD_SS)|(1<<DD_SCK));
   
    SPCR = ((1<<SPE)|               // SPI Enable
           (0<<SPIE)|              // SPI Interupt Enable
           (0<<DORD)|              // Data Order (0:MSB first / 1:LSB first)
           (1<<MSTR)|              // Master/Slave select   
           (0<<SPR1)|(1<<SPR0)|    // SPI Clock Rate
           (0<<CPOL)|              // Clock Polarity (0:SCK low / 1:SCK hi when idle)
           (0<<CPHA));             // Clock Phase (0:leading / 1:trailing edge sampling)
 
}

void spi_driver_transmit (uint8_t * dataout, uint8_t * datain, uint8_t len)
{
    uint8_t i;      
    for (i = 0; i < len; i++) 
    {
        SPDR = dataout[i];
        while((SPSR & (1<<SPIF))==0);
        datain[i] = SPDR;                   
    }
}