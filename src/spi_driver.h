#include <avr/io.h>

void spi_driver_init_master(void);
void spi_driver_transmit(uint8_t *dataout, uint8_t *datain, uint8_t len);